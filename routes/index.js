const express = require('express')
const router = express.Router()
const Prismic = require('prismic-javascript')
const { apiEndpoint, apiToken} = require('../config/keys')


/* GET users listing. */
router.get('/', function(req, res, next) {
    Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then(function(api) {
         api.getSingle('homepage').then((page) => {
          
          res.render('index', {
            page: { 
              title: page.title, 
              active_section: 'home',
              content: page 
            }
          })
        })
       })
       
       
})

router.get('/:slug', function(req,res,next) {
    Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then((api) => {
      api.getByUID('page', req.params.slug).then((page) => {
        api.query(Prismic.Predicates.at('document.type', 'page'), { orderings : '[my.page.order]' }).then((pages) => {

          
        


          if(page === undefined) {
            next()
          } else {
            
            res.locals.page = {
                  title: (page && page.data && page.data.title && page.data.seo_title) ? page.data.seo_title : '',
                  active_section: 'our-story',
                  active_page: req.params.slug,
                  content: page,                  
                  links: pages.results.map((p) => { return { title: p.data.title[0].text, slug: p.uid }})
                  
              }   
            res.render('page')
          }
        })
        
      })
    })

})

module.exports = router
