var express = require('express')
var router = express.Router()
const Prismic = require('prismic-javascript')
const { apiEndpoint, apiToken} = require('../config/keys')


/* GET users listing. */
router.get('/', function(req, res, next) {
    Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then((api) => {
        return api.query(Prismic.Predicates.at('document.type', 'project_category'), {orderings: '[my.project_category.order]'}) // An empty query will return all the documents
    }).then((response) => {
        
        Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then(function(api) {
            return api.query(Prismic.Predicates.at('document.type', 'project'), { pageSize : 100 })
        }).then((page) => {
            
           res.locals.page = {
                title: 'Metro Vancouver Construction Project Portfolio by KDS Construction Ltd',
                active_section: 'projects',
                active_page: response.results[0].uid,
                content: response.results,
                projects: page.results
            }
            
            res.render('projects' )
        }, (err) => {
            res.err('something went wrong')
            console.log("Something went wrong: ", err)
        })        
    }, function(err) {
        res.err('something went wrong')
        console.log("Something went wrong: ", err)
    }) 
})

router.get('/:type', (req, res, next) => {
    Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then(function(api) {
         api.getByUID('project_category', req.params.type).then((type_page) => {
            

            Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then(function(api) {
                return api.query(Prismic.Predicates.at('document.type', 'project'))
            }).then((page) => {

               
               res.locals.page = {
                title: type_page.data.project_type[0].text+' Construction Management in Metro Vancouver and Fraser Valley - KDS Construction Ltd',
                active_section: 'projects',                
                content: type_page,
                projects: page.results.filter(p => p.data.project_type.uid == req.params.type)
            }   
                res.render('project_type')
               
            }, (err) => {
                res.err('something went wrong')
                console.log("Something went wrong: ", err)
            })        




            
        })
       })
})

router.get('/:type/:project', (req, res, next) => {
    
    Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then(function(api) {
         api.getByUID('project', req.params.project).then((page) => {
            
            res.locals.page = {
                title: '',
                active_section: 'projects',
                active_page: req.params.project,
                content: page
            }   
            res.render('project')
        })
       })
    

})

module.exports = router
