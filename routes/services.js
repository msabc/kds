var express = require('express')
var router = express.Router()
const Prismic = require('prismic-javascript')
var PrismicDOM = require('prismic-dom')
const { apiEndpoint, apiToken} = require('../config/keys')


/* GET users listing. */
router.get('/', function(req, res, next) {
    Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then(function(api) {
         return api.query(Prismic.Predicates.at('document.type', 'service'), { orderings: '[my.service.order]'}) // An empty query will return all the documents
       }).then(function(response) {
        
         res.render('services', {
            page: {
                title: response.results[0].data.title[0].text+' Services in Metro Vancouver & the Fraser Valley by KDS Construction Ltd',
                active_section: 'services',
                active_page: response.results[0].uid,
                content: {
                    title: response.results[0].data.title[0].text,
                    body: PrismicDOM.RichText.asHtml(response.results[0].data.body),
                    img: response.results[0].data.primary_photo.url
                }, 
                links: response.results.map((s) => { return { title: s.data.title[0].text, slug: s.uid} })
            }
         })
       }, function(err) {
        res.err('something went wrong')
         console.log("Something went wrong: ", err)
       })
       
       
})

router.get('/:service', function(req, res, next) {
    Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then(function(api) {
         return api.query(Prismic.Predicates.at('document.type', 'service'), { orderings: '[my.service.order]'}) // An empty query will return all the documents
       }).then(function(response) {
        response.results.forEach((p) => {
            if(p.uid == req.params.service) {
                res.render('services', {
                    page: {
                        title: p.data.title[0].text+' Services in Metro Vancouver & the Fraser Valley by KDS Construction Ltd',
                        active_section: 'services',
                        active_page: req.params.service,
                        content: {
                            title: p.data.title[0].text,
                            body: PrismicDOM.RichText.asHtml(p.data.body),
                            img: p.data.primary_photo.url
                        }, 
                        links: response.results.map((s) => { return { title: s.data.title[0].text, slug: s.uid} })
                    }
                })
            }

            
        })
       }, function(err) {
        res.err('something went wrong')
         console.log("Something went wrong: ", err)
       })






       
       
})

module.exports = router
