var express = require('express')
var router = express.Router()
const Prismic = require('prismic-javascript')
const { apiEndpoint, apiToken} = require('../config/keys')

router.get('/', function(req, res, next) {
    Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then(function(api) {
         return api.query(Prismic.Predicates.at('document.type', 'letter_of_recommendation'), { orderings : '[my.letter_of_recommendation.year desc]' }) // An empty query will return all the documents
    }).then(function(response) {
        res.locals.page = {
            title: 'Letters of Recommendation regarding KDS Construction Ltd.',
            active_section: 'our-story',
            active_page: response.results[0].uid,
            content: response.results
        }   

        res.render('lor')

    }, function(err) {
        res.err('something went wrong')
         console.log("Something went wrong: ", err)
    })      
       
})

router.get('/:slug', function(req, res, next) {
    Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then(function(api) {
         return api.query(Prismic.Predicates.at('document.type', 'letter_of_recommendation'), { orderings : '[my.letter_of_recommendation.year desc]' }) // An empty query will return all the documents
    }).then(function(response) {
        
        res.locals.page = {
            title: response.results.filter(p => p.uid == req.params.slug).shift().data.body[0].text+', '+response.results.filter(p => p.uid == req.params.slug).shift().data.title[0].text+' recommends KDS Construction Ltd.',
            active_section: 'our-story',
            active_page: req.params.slug,
            content: response.results
        }   

        res.render('lor')

    }, function(err) {
        res.err('something went wrong')
         console.log("Something went wrong: ", err)
    })      
       
})

module.exports = router
