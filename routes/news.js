var express = require('express')
var router = express.Router()
const Prismic = require('prismic-javascript')
const { apiEndpoint, apiToken} = require('../config/keys')


/* GET users listing. */
router.get('/', function(req, res, next) {
    Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then(function(api) {
         return api.query(Prismic.Predicates.at('document.type', 'news'), { orderings : '[my.news.year desc]' }) // An empty query will return all the documents
    }).then(function(response) {
        
        res.locals.page = {
            title: 'News from KDS Construction Ltd',
            active_section: 'our-story',
            active_page: response.results[0].uid,
            content: response.results
        }   

        res.render('news')

    }, function(err) {
        res.err('something went wrong')
         console.log("Something went wrong: ", err)
    })      
       
})

/* GET users listing. */
router.get('/:story', function(req, res, next) {
    Prismic.getApi(apiEndpoint, {accessToken: apiToken}).then(function(api) {
         return api.query(Prismic.Predicates.at('document.type', 'news'), { orderings : '[my.news.year desc]' }) // An empty query will return all the documents
    }).then(function(response) {
        
        res.locals.page = {
            title: response.results.filter(p => p.uid == req.params.story).shift().data.title[0].text,
            active_section: 'our-story',
            active_page: req.params.story,
            content: response.results
        }   

        res.render('news')

    }, function(err) {
        res.err('something went wrong')
         console.log("Something went wrong: ", err)
    })      
       
})

module.exports = router
